package com.ajit.service;

import com.ajit.model.Employee;
import com.ajit.repository.EmployeeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

    @InjectMocks
    EmployeeService employeeService;

    @Mock
    EmployeeRepository employeeRepository;

    @Test
    public void save() {
        Employee employee = new Employee();
        employee.setName("Mohan");
        employeeService.save(employee);
    }

    @Test
    public void findByEmpIdTest() {
        String empId = "101";
        Employee employee = new Employee();
        employee.setName("Mohan");
        Mockito.when(employeeRepository.findByEmpId(empId)).thenReturn(employee);
        employeeService.findByEmpId(empId);
    }

    @Test
    public void findAllEmployeeTest() {
        List<Employee> employees = new ArrayList<>();
        Employee emp1 = new Employee();
        emp1.setEmpId("101");
        employees.add(emp1);

        Mockito.when(employeeRepository.findAll()).thenReturn(employees);
        employeeService.findAllEmployee();
    }

    @Test
    public void update() {
        Employee employee = new Employee();
        employee.setEmpId("101");
        employee.setDob("28/02/87");
        employee.setAge(22);
        employee.setSalary("80000");

        Mockito.when(employeeRepository.findByEmpId(employee.getEmpId())).thenReturn(employee);
        Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
        employeeService.update(employee);
    }
}
