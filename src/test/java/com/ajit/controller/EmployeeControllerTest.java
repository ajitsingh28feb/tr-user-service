package com.ajit.controller;

import com.ajit.exception.DataNotFoundException;
import com.ajit.exception.RecordNotFoundException;
import com.ajit.info.RuleResponse;
import com.ajit.model.Employee;
import com.ajit.service.EmployeeService;
import com.ajit.util.RuleUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeControllerTest {

    @InjectMocks
    ReportController employeeController;

    @Mock
    EmployeeService employeeService;

    @Mock
    RuleUtil employeeUtil;

    @Mock
    HttpServletRequest request;

    @Mock
    ResponseEntity reqResponseEntity;

    @Mock
    RuleResponse employeeResponse;

    @Test
    public void createEmployeeTest() {
        Employee employee = new Employee();
        employee.setAge(22);
        employee.setEmpId("101");
        employee.setDob("28/02/87");
        employee.setName("ABC");
        employee.setSalary("800000");
        request.setAttribute("fileType", "csv");

        Mockito.when(employeeService.save(employee)).thenReturn(employee);
        employeeController.createEmployee(employee, request);

        request.setAttribute("fileType", "xml");
        Mockito.when(employeeService.save(employee)).thenReturn(employee);
        employeeController.createEmployee(employee, request);
    }

    @Test(expected = DataNotFoundException.class)
    public void createEmployeeExceptionTest() {
        Employee employee = new Employee();
        Mockito.when(employeeService.save(employee)).thenReturn(employee);
        employeeController.createEmployee(employee, request);
    }

    @Test(expected = DataNotFoundException.class)
    public void employeeExceptionTest() {
        Employee employee = new Employee();
        employee.setName("Mohan");
        Mockito.when(employeeService.save(employee)).thenReturn(employee);
        employeeController.createEmployee(employee, request);
        employee.setAge(22);
        Mockito.when(employeeService.save(employee)).thenReturn(employee);
        employeeController.createEmployee(employee, request);
        employee.setDob("28/02/87");
        Mockito.when(employeeService.save(employee)).thenReturn(employee);
        employeeController.createEmployee(employee, request);
        employee.setSalary("80000");
        Mockito.when(employeeService.save(employee)).thenReturn(employee);
        employeeController.createEmployee(employee, request);

    }

    @Test
    public void updateEmpTest() {
        Employee employee = new Employee();
        employee.setEmpId("101");
        employee.setDob("28/02/87");
        employee.setAge(22);
        employee.setSalary("80000");
        employeeController.updateEmp(employee, request);

        employee = null;
        employeeController.updateEmp(employee, request);
    }

    @Test
    public void getEmployeeTest() {
        List<Employee> employees = new ArrayList<>();
        Employee emp1 = new Employee();
        emp1.setEmpId("101");
        emp1.setName("Mohan");

        Employee emp2 = new Employee();
        emp2.setEmpId("102");
        emp2.setName("Sohan");

        employees.add(emp1);
        employees.add(emp2);

        Mockito.when(employeeService.findAllEmployee()).thenReturn(employees);
        employeeController.getEmployee();

        Mockito.when(employeeService.findAllEmployee()).thenReturn(null);
        employeeController.getEmployee();
    }

    @Test
    public void getEmployeeByIdTest() {
        String empId = "101";
        Employee employee = new Employee();
        employee.setName("Mohan");
        Mockito.when(employeeService.findByEmpId(empId)).thenReturn(employee);
        employeeController.getEmployeeById(empId);

        Mockito.when(employeeService.findByEmpId(empId)).thenReturn(null);
        employeeController.getEmployeeById(empId);
    }

}
