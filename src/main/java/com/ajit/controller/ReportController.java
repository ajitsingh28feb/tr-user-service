package com.ajit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ajit.deserialize.ReportInfoI;
import com.ajit.info.CommonReportInfo;
import com.ajit.info.RuleReportInfo;
import com.ajit.service.RuleReportService;

@RestController
@RequestMapping("/v1/report")
public class ReportController {
	
	@Autowired
	CommonReportInfo commonReportInfo;
	
	@Autowired
	RuleReportService ruleReportService;
	
	/*
	 * {
	 * 		"reportType":"RuleReport",
	 * 		"reportInfo": {
	 * 			"startDate":"2021-10-05",
	 * 			"ensDate":"2021-10-12",
	 * 			"orgId":10010,
	 * 			"clientName":"Altius-Dev-1",
	 * 			"clientId":"0678581477"
	 * 		}
	 * }
	 */
	/**
	 * 
	 * @param commonReportInfo
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@PostMapping(value="/execution", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity executeFilteredReport(@RequestBody CommonReportInfo commonReportInfo) {
		ReportInfoI reportInfoI = commonReportInfo.getReportInfo();
		
		try {
			if(reportInfoI instanceof RuleReportInfo) {
				return ResponseEntity.ok(ruleReportService.executeRuleReport("ruleReport", (RuleReportInfo) reportInfoI));
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
}
