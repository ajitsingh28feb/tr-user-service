package com.ajit.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ajit.info.BatchJobResult;
import com.ajit.model.BatchConfigData;
import com.ajit.model.BatchStep;

@Component
public class BatchProcessMultiJob<T> {

	@Autowired
	JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	BatchJobListener batchJobListener;
	
	@Autowired
	StepBuilderFactory stepBuilderFactory;
	
	public BatchJobResult batchMultiJob(BatchConfigData configData) {
		BatchJobResult jobResult = new BatchJobResult();
		Job job = ((JobBuilder) ((JobBuilder)this.jobBuilderFactory.get(configData.getJobName()).listener(this.batchJobListener)).incrementer(new RunIdIncrementer())).start(flow)
	}
	
	private Step startStep(String stepName,  BatchStep batchStep, BatchConfigData configData) {
		return this.stepBuilderFactory.get(stepName).tasklet((Tasklet)(this.isStepProcess(batchStep) ? "step" : "stepVal").listener(this.batchStepListener(batchStep) ? configData.getThreadChunkVal()).reader(this.isStepProcess(batchStep) ? this.ite..batchStep..))
	}
	
	private boolean isStepProcess(BatchStep batchStep) {
		return BatchStep.PROCESS.equals(batchStep);
	}
}
