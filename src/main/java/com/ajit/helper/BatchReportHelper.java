package com.ajit.helper;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ajit.info.BatchInputInfo;
import com.ajit.info.BatchJobResult;
import com.ajit.info.BatchJobType;
import com.ajit.info.BatchResult;
import com.ajit.info.BatchStatus;
import com.ajit.model.BatchReport;
import com.ajit.model.BatchReportData;
import com.ajit.model.BatchSequenceGenerator;
import com.ajit.model.BatchTracker;
import com.ajit.service.BatchReportService;
import com.ajit.service.BatchTrackerService;
import com.ajit.util.BatchHelper;

@Service
public class BatchReportHelper<T> {
	
	@Autowired
	BatchSequenceGenerator sequenceGenerator;
	
	@Autowired
	BatchReportService reportService;
	
	@Autowired
	BatchTrackerService trackerService;
	
	@Autowired
	BatchHelper batchHelper;
	
	@Value("${batch.report.exist.check:false}")
	private boolean batchReportExistCheck;

	public BatchReport initBatchReportDetails(BatchInputInfo batchInputInfo, T t) {
		BatchReport batchReport = new BatchReport();
		batchReport.setOrgId(batchInputInfo.getOrgId());
		batchReport.setUser(batchInputInfo.getUser());
		batchReport.setClientName(batchInputInfo.getClientName());
		batchReport.setBusinessUnit(batchInputInfo.getBusinessUnit());
		batchReport.setReportName(batchInputInfo.getJobName());
		batchReport.setReportType(batchInputInfo.getJobDisplayName());
		batchReport.setRecordCount(batchInputInfo.getRecordCount());
		batchReport.setBatchStepData(new HashMap());
		batchReport = this.createBatchReport(batchReport, BatchStatus.RECEIVED);
		if(!ObjectUtils.isEmpty(batchReport)) {
			this.createBatchReportData(batchReport, t);
		}
		
		return batchReport;
	}
	
	public BatchReport createBatchReport(BatchReport batchReport, BatchStatus status) {
		batchReport.setReportId(this.sequenceGenerator.generateSequence("batchReport_sequence"));
		batchReport.setStatus(status);
		
		return this.reportService.saveBatchReport(batchReport);
	}
	
	@SuppressWarnings("unchecked")
	public BatchReport createBatchReportData(BatchReport batchReport, T obj) {
		@SuppressWarnings("rawtypes")
		BatchReportData reportData = new BatchReportData();
		reportData.setReportDataId(this.sequenceGenerator.generateSequence("batchReportData_sequence"));
		reportData.setReportId(batchReport.getReportId());
		reportData.setData(obj);
		reportData.setDataId(obj.toString());
		reportData.setCrtTs(batchReport.getCrtTs());
		reportData.setUpdTs(batchReport.getUpdTs());
		
		return this.reportService.saveBatchReport(batchReport);
	}
	
	public BatchJobResult errorBatchJobResult(BatchJobResult jobResult, String message) {
		jobResult.setSuccess(false);
		jobResult.setMessage(message);
		return jobResult;
	}
	
	public BatchReport updateBatchReport(long uploadId, BatchStatus status) {
		BatchReport batchReport = new BatchReport();
		batchReport.setStatus(status);
		
		if(Arrays.asList(BatchStatus.COMPLETE, BatchStatus.ERROR).contains(status)) {
			batchReport.setEndDateTime(LocalDateTime.now());
		}
		
		batchReport.setUpdTs(LocalDateTime.now());
		return this.reportService.saveBatchReport(batchReport);
	}
	
	public boolean processBatchReportExist(BatchReport batchReport, T t) {
		if(!this.batchReportExistCheck) {
			return true;
		} else {
			BatchReportData batchReportData = this.reportService.findReportDataByDataId(t.toString());
			if(ObjectUtils.isEmpty(batchReportData)) {
				return true;
			} else {
				BatchReport existingBatchReport = this.reportService.findById(batchReportData.getReportId());
				if(!ObjectUtils.isEmpty(existingBatchReport) && BatchStatus.COMPLETE.equals(existingBatchReport.getStatus())) {
					BatchTracker batchTracker = this.trackerService.findByReportId(batchReportData.getReportId());
					
					if(ObjectUtils.isEmpty(batchTracker)) {
						return true;
					} else {
						this.createBatchTrackerForReportExists(batchTracker, batchReport);
						this.updateBatchTrackerForReportExists(batchReport, existingBatchReport);
						return false;
					}
				} else {
					return true;
				}
			}
		}
	}
	
	private void updateBatchTrackerForReportExists(BatchReport batchReport, BatchReport existingBatchReport) {
		batchReport.setStatus(existingBatchReport.getStatus());
		batchReport.setResult(existingBatchReport.getResult());
		batchReport.setRecordCount(existingBatchReport.getRecordCount());
		batchReport.setProcessCount(existingBatchReport.getProcessCount());
		this.reportService.update(batchReport);
		
	}

	private void createBatchTrackerForReportExists(BatchTracker batchTracker, BatchReport batchReport) {
		batchTracker.setReportId(batchReport.getReportId());
		batchTracker.setOrg(batchReport.getOrgId().toString());
		batchTracker.setStatus(BatchStatus.COMPLETE);
		batchTracker.setResult(BatchResult.SUCCESS);
		batchTracker.setJobType(BatchJobType.HISTORY);
		this.batchHelper.createJobTracker(batchTracker);
	}
	
	public BatchJobResult successBatchJobResult(BatchJobResult jobResult, String message) {
		jobResult.setSuccess(true);
		jobResult.setMessage(message);
		return jobResult;
	}
}
