package com.ajit.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ajit.model.BatchReport;
import com.ajit.model.BatchReportData;
import com.ajit.repository.BatchReportRepository;

@Service
public class BatchReportService {

	@Autowired
	BatchReportRepository batchReportRepository;
	
	public BatchReport saveBatchReport(BatchReport batchReport) {
		try {
			if(ObjectUtils.isEmpty(batchReport.getCrtTs())) {
				batchReport.setCrtTs(LocalDateTime.now());
			}
			return this.batchReportRepository.save(batchReport);
		} catch (Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public BatchReportData findReportDataByDataId(String dataId) {
		return this.batchReportRepository.findFirstByDataId(dataId);
	}
	
	public BatchReport update(BatchReport batchReport) {
		try {
			batchReport.setUpdTs(LocalDateTime.now());
			return (BatchReport)this.batchReportRepository.save(batchReport);
		} catch (Exception e) {
			return null;
		}
	}
}
