package com.ajit.service;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ajit.model.BatchUpload;
import com.ajit.repository.BatchUploadRepository;

@Service
public class BatchUploadService {

	@Autowired
	BatchUploadRepository batchUploadRepository;
	
	public BatchUpload findByAll(long uploadId) {
		Optional<BatchUpload> batchUpload = this.batchUploadRepository.findById(uploadId);
		return batchUpload.isPresent() ? (BatchUpload)batchUpload.get():null;
	}
	
	public BatchUpload save(BatchUpload batchUpload) {
		try {
			if(ObjectUtils.isEmpty(batchUpload.getCrtTs())) {
				batchUpload.setCrtTs(LocalDateTime.now());
			}
			
			if(ObjectUtils.isEmpty(batchUpload.getCrtTs())) {
				batchUpload.setCrtTs(LocalDateTime.now());
			}
			
			return this.batchUploadRepository.save(batchUpload);
		} catch (Exception e) {
			return null;
		}
	}
}
