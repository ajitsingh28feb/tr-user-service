package com.ajit.service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ajit.helper.BatchReportHelper;
import com.ajit.info.BatchAction;
import com.ajit.info.BatchJob;
import com.ajit.info.BatchJobResult;
import com.ajit.info.BatchJobType;
import com.ajit.info.BatchResult;
import com.ajit.info.BatchRunType;
import com.ajit.info.BatchStatus;
import com.ajit.info.BatchTypeField;
import com.ajit.job.BatchProcessMultiJob;
import com.ajit.model.BatchConfig;
import com.ajit.model.BatchConfigData;
import com.ajit.model.BatchConfigField;
import com.ajit.model.BatchTracker;
import com.ajit.model.BatchUpload;
import com.ajit.util.BatchHelper;
import com.ajit.util.BatchUploadHelper;

@Service
public class BatchJobService {
	static DateTimeFormatter formatter;
	@Autowired
	JobService jobService;
	
	@Autowired
	JobFieldService jobFieldService;
	
	@Autowired
	BatchHelper batchHelper;
	
	@Autowired
	BatchTracker batchTracker;
	
	@Autowired
	BatchJobService batchJobService;
	
	@Autowired
	BatchUploadService batchUploadService;
	
	@Autowired
	BatchProcessMultiJob batchProcessMultiJob;
	
	@Autowired
	BatchUploadHelper uploadHelper;
	
	@Autowired
	BatchReportHelper reportHelper;
 	
	
	@Value("${batch.upload.filePath}")
	private String batchUploadFilePath;

	public BatchJobResult executeBatchJob(BatchJob batchJob) {
		BatchJobResult jobResult = null;
		BatchConfigData configData;
		BatchTracker batchTracker;
		
		try {
			BatchConfig batchConfig = this.jobService.findBatchConfigByJobName(batchJob.getName());
			batchTracker = this.initJobTracker(batchConfig, batchJob);
			configData = this.loadBatchConfigData(batchJob, batchConfig, 100L);
			if(configData.getWriteParameter().get(BatchTypeField.FILENAME.name()) != null) {
				this.batchHelper.updateJobTracker(batchTracker.getTrackId(), (BatchStatus)null, (String)configData.getWriteParameter().get(BatchTypeField.FILENAME.name()));
			}
			
			if(batchJob.isBatchMultiJob()) {
				this.processUploadFile(batchJob, configData);
				jobResult = this.batchProcessMultiJob.batchMultiJob(configData);
			} else {
				//
			}
		} catch (Exception e) {
			jobResult = new BatchJobResult();
			jobResult.setSuccess(false);
			jobResult.setMessage("Error");
			return jobResult;
		}
		
		if(configData.getRunType().equals(BatchRunType.INLINE)) {
			batchTracker = this.batchHelper.findById(batchTracker.getTrackId());
			jobResult = jobResult != null ? jobResult : new BatchJobResult();
			jobResult.setSuccess(batchTracker.getResult().equals(BatchResult.SUCCESS));
			jobResult.setMessage(batchTracker.getResult().equals(BatchResult.SUCCESS) ? "DONE" : "Errro");
		}
		return jobResult;
	}
	
	private BatchConfigData loadBatchConfigData(BatchJob batchJob, BatchConfig batchConfig, long jobTrackId) throws IOException {
		BatchConfigData configData = new BatchConfigData();
		configData.setJobName(batchJob.getName());
		configData.setBatchTrackId(jobTrackId);
		configData.setThreadCount(batchConfig.getThreads());
		configData.setThreadCountVal(batchConfig.getThreadsVal());
		configData.setThreadChunk(batchConfig.getChunks());
		configData.setThreadChunkVal(batchConfig.getChunksVal());
		configData.setStatus(batchConfig.getStatus());
		
		List<BatchConfigField> jobFields = this.jobFieldService.findAllByJobName(batchJob.getName());
		this.addJobParameter(jobFields, BatchTypeField.TRACKID, String.valueOf(jobTrackId));
		this.processJobParameters(configData, jobFields);
		
		if(BatchJobType.EXPORT.equals(batchJob.getJobType())) {
			this.processExportJobParameter(configData, batchJob);
		} else if(BatchJobType.HISTORY.equals(batchJob.getJobType())) {
			configData.setReportId(batchJob.getReportId());
			configData.setRecordCount(batchJob.getRecordCount());
		} else if(BatchJobType.UPLOAD.equals(batchJob.getJobType())) {
			configData.setUploadId(batchJob.getUploadId());
		}
		
		if(batchJob.getQuery() != null) {
			configData.setQuery(batchJob.getQuery());
		}
		
		if(batchJob.getQueryModel() != null) {
			configData.setQueryModel(batchJob.getQueryModel());
		}
		
		if(batchJob.getCriteria() != null) {
			configData.setCriteriaParameter(batchJob.getCriteria());
		}
		
		return configData;
	}
	
	private void addJobParameter(List<BatchConfigField> jobFields, BatchTypeField field, String value) {
		BatchConfigField jobField = new BatchConfigField();
		jobField.setField(field);
		jobField.setValue(value);
		jobFields.add(jobField);
	}
	
	private void processJobParameters(BatchConfigData configData, List<BatchConfigField> jobFields) throws IOException {
		List<Map<String, String>> jobParms = new ArrayList();
		Map<String, String> readerParms  = new HashMap();
		Map<String, String> writersParms  = new HashMap();
		this.processParams(configData, jobFields, jobParms, readerParms, writersParms);
		boolean createTempFile = writersParms.get(BatchTypeField.FILENAME.name()) != null && writersParms.get(BatchTypeField.FORMAT.name()) !=null;
		if(createTempFile) {
			File tempFile = File.createTempFile((String)writersParms.get(BatchTypeField.FILENAME.name()), (String)writersParms.get(BatchTypeField.FORMAT.name()));
			if(tempFile != null && tempFile.exists()) {
				Map<String, String> fieldMap = new HashMap<>();
				fieldMap.put(BatchTypeField.FILEPATH.name(), tempFile.getAbsolutePath());
				jobParms.add(fieldMap);
				writersParms.put(BatchTypeField.FILEPATH.name(), tempFile.getAbsolutePath());
			}
		}
		
		configData.setBatchParameters(jobParms);
		configData.setReadParameter(readerParms);
		configData.setWriteParameter(writersParms);
	}
	
	private void processUploadFile(BatchJob batchJob, BatchConfigData configData) throws IOException {
		if(batchJob.isReadSPOS()) {
			BatchUpload batchUpload = this.batchUploadService.findByAll(batchJob.getUploadId());
			if(ObjectUtils.isEmpty(batchUpload)) {
				// throw new BatchUploadException(ErrorCode.DATA_SERVICE_ERROR, "upload ID not found");
			}
			
			String sposId = (ObjectUtils.isEmpty(batchUpload.getOrgId()) ? "SYSTEM" : batchUpload.getOrgId() + "/" + batchUpload.getUser() + "/UPLOAD/" + batchUpload.getUploadName() + "/" + batchUpload.getUuid());
			Resource uploadResource = this.getFileResource(sposId, batchUpload.getFileName(), this.batchUploadFilePath);
			
			if(!ObjectUtils.isEmpty(uploadResource)) {
				configData.setInputResource(uploadResource);
			}
		} else {
			//do nothing
		}
	}
	
	public Resource getFileResource(String sposId, String fileName, String destination) throws IOException {
		//return resource file 
		return null;
	}
	
	private void processExportJobParameter(BatchConfigData batchConfigData, BatchJob batchJob) {
		batchConfigData.setUploadId(batchJob.getUploadId());
		BatchConfigField objField = this.jobFieldService.findByJobNameActionFieldType(batchJob.getExportObjJobName(), BatchAction.UPLOAD, BatchTypeField.HEADER);
		
		if(!ObjectUtils.isEmpty(objField)) {
			batchConfigData.getWriteParameter().put(BatchTypeField.FIELDS.name(), objField.getValue());
		}
		
		BatchConfigField headerField = this.jobFieldService.findByJobNameActionFieldType(batchJob.getExportObjJobName(), BatchAction.UPLOAD, BatchTypeField.HEADER);
		if(!ObjectUtils.isEmpty(headerField)) {
			batchConfigData.getWriteParameter().put(BatchTypeField.HEADER.name(), headerField.getValue());
		}
	}
	
	private BatchTracker initJobTracker(BatchConfig batchConfig, BatchJob batchJob) {
		BatchTracker batchTracker = new BatchTracker();
		batchTracker.setJobId(batchConfig.getJobId());
		batchTracker.setJobName(batchConfig.getJobName());
		batchTracker.setStatus(BatchStatus.STARTED);
		batchTracker.setRunType(batchConfig.getRunType());
		
		if(BatchJobType.UPLOAD.equals(batchJob.getJobType())) {
			batchTracker.setUploadId(batchJob.getUploadId());
			this.uploadHelper.updateBatchUpload(batchJob.getUploadId(), BatchStatus.STARTED);
		} else if(BatchJobType.HISTORY.equals(batchJob.getJobType())) {
			batchTracker.setReportId(batchJob.getReportId());
			this.reportHelper.updateBatchReport(batchJob.getReportId(), BatchStatus.STARTED);
		}
		batchTracker = this.batchHelper.createJobTracker(batchTracker);
		return batchTracker;
	}
	
	@SuppressWarnings("rawtypes")
	private void processParams(BatchConfigData configData, List<BatchConfigField> jobFields, List<Map<String, String>> jobParams, Map<String, String> readerParms, Map<String, String> writerParms) {
		Iterator var = jobFields.iterator();
		while(var.hasNext()) {
			BatchConfigField field = (BatchConfigField)var.next();
			Map<String, String> fieldMap = new HashMap<>();
			if(!ObjectUtils.isEmpty(field.getAction()) && field.getAction().equals(BatchAction.READER)) {
				readerParms.put(field.getField().name(), field.getValue());
			} else if(!ObjectUtils.isEmpty(field.getAction()) && field.getAction().equals(BatchAction.WRITER)) {
				if(field.getField().equals(BatchTypeField.FILENAME)) {
					field.setValue(field.getValue()+"_"+LocalDateTime.now().format(formatter).replace(":", ".")+"_"+configData.getUser());
				}
				writerParms.put(field.getField().name(), field.getValue());
			} else if(!ObjectUtils.isEmpty(field.getAction()) && field.getAction().equals(BatchAction.FIELD_MAPPER)) {
				configData.setFieldMapper(field.getValue());
			} else if(!ObjectUtils.isEmpty(field.getAction()) && field.getAction().equals(BatchAction.PROCESSOR)) {
				configData.setProcessor(field.getValue());
			}
			
			fieldMap.put(field.getField().name(), field.getValue());
			jobParams.add(fieldMap);
		} 
	}
}
