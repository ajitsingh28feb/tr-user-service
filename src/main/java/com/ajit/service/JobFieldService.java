package com.ajit.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.ajit.info.BatchAction;
import com.ajit.info.BatchTypeField;
import com.ajit.model.BatchConfigField;

@Service
public class JobFieldService {

	@Autowired
	MongoTemplate mongoTemplate;
	
	public List<BatchConfigField> findAllByJobName(String jobName) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("jobName").is(jobName));
			return this.mongoTemplate.find(query, BatchConfigField.class);
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}
	
	public BatchConfigField findByJobNameActionFieldType(String jobName, BatchAction batchAction, BatchTypeField fieldType) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("jobName").is(jobName));
			query.addCriteria(Criteria.where("action").is(batchAction));
			query.addCriteria(Criteria.where("field").is(fieldType));
			
			return (BatchConfigField)this.mongoTemplate.findOne(query, BatchConfigField.class);
		} catch (Exception e) {
			return null;
		}
	}
}
