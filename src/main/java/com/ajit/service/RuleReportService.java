package com.ajit.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ajit.adapter.RuleBatchExecution;
import com.ajit.exception.ReportValidationException;
import com.ajit.info.BatchJobResult;
import com.ajit.info.RuleReportInfo;
import com.ajit.util.EmsReportUtil;

@Service
public class RuleReportService extends RuleBatchExecution<RuleReportInfo> {
	
	@Autowired
	EmsReportUtil emsReportUtil;
	
	public BatchJobResult executeRuleReport(String reportName, RuleReportInfo ruleReportInfo) {
		BatchJobResult jobResult;
		try {
			validateRestrictRuleReport(ruleReportInfo);
			return execute(EmsReportUtil.initBatchInputInfo(reportName, "rule", ruleReportInfo.getOrgId(), ruleReportInfo.getClientName()), ruleReportInfo);
		} catch (Exception e) {
			jobResult = new BatchJobResult();
			jobResult.setMessage(e.getMessage());
			jobResult.setSuccess(false);
			return jobResult;
		}
	}

	private boolean validateRestrictRuleReport(RuleReportInfo ruleReportInfo) {
		validateStartEndDate(ruleReportInfo.getStartDate(), ruleReportInfo.getEndDate());
		return true;
	}

	private void validateStartEndDate(LocalDate startDate, LocalDate endDate) {
		if(startDate.isAfter(LocalDate.now())) {
			throw new ReportValidationException("Invalid Start Date");
		}
		if(ChronoUnit.MONTHS.between(startDate, LocalDate.now()) > 18) {
			throw new ReportValidationException("Start Date must be 18 Month or less");
		}
		if(endDate.isAfter(LocalDate.now()) || endDate.isBefore(startDate)) {
			throw new ReportValidationException("Invalid Start Date");
		}
		if(ChronoUnit.DAYS.between(startDate, endDate) > 90) {
			throw new ReportValidationException("Invalid start and end date range");
		}
	}

	@Override
	public Query queryBuilder(RuleReportInfo ruleReportInfo) {
		Query query = new Query();
		query.addCriteria(Criteria.where("orgId").is(ruleReportInfo.getOrgId()));
		query.addCriteria(Criteria.where("updTs")
				.gte(java.util.Date.from(LocalDateTime.of(ruleReportInfo.getStartDate(), LocalTime.MIN).atZone(ZoneId.systemDefault()).toInstant()))
				.lte(java.util.Date.from(LocalDateTime.of(ruleReportInfo.getEndDate(), LocalTime.MIN).atZone(ZoneId.systemDefault()).toInstant())));
		
		if(!ObjectUtils.isEmpty(ruleReportInfo.getRuleName())) {
			query.addCriteria(Criteria.where("ruleName").is(ruleReportInfo.getRuleName()));
		}
		
		return query;
		
	}

	@Override
	public Object queryBuilderModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> criteriaBuilder(RuleReportInfo ruleReportInfo) {
		return new HashMap<>();
	}

}
