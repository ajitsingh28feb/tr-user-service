package com.ajit.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajit.model.BatchTracker;
import com.ajit.repository.BatchTrackerRepository;

@Service
public class BatchTrackerService {

	@Autowired
	BatchTrackerRepository batchTrackerRepository;
	
	
	public BatchTracker findById(long runId) {
		Optional<BatchTracker> runTracker = this.batchTrackerRepository.findById(runId);
		return runTracker.isPresent() ? (BatchTracker)runTracker.get() : null;
	}
	
	public BatchTracker save(BatchTracker runTracker) {
		try {
			return (BatchTracker)this.batchTrackerRepository.save(runTracker);
		} catch (Exception e) {
			return null;
		}
	}
	
	public BatchTracker findByReportId(Long reportId) {
		return this.batchTrackerRepository.findByReportId(reportId);
	}
}
