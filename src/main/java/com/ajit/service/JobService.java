package com.ajit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajit.info.BatchStatus;
import com.ajit.model.BatchConfig;
import com.ajit.repository.BatchConfigRepository;

@Service
public class JobService {
	@Autowired
	BatchConfigRepository batchConfigRepository;
	
	public BatchConfig findBatchConfigByJobName(String jobName) {
		return this.batchConfigRepository.findByJobNameAndStatus(jobName, BatchStatus.ACTIVE.name());
	}
}
