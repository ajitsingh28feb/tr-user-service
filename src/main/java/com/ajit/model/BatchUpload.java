package com.ajit.model;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.ajit.info.BatchResult;
import com.ajit.info.BatchRunType;
import com.ajit.info.BatchStatus;
import com.ajit.info.BatchUserType;

@Document(collection = "batchUpload")
public class BatchUpload extends BaseDataObj {
	@Transient
	public static final String SEQUENCE_NAME = "batchUpload_sequence";
	@Id
	private long uploadId;
	@NotNull
	private String uploadName;
	private String fileName;
	private BatchStatus status;
	private BatchRunType runType;
	private BatchResult result;
	private Long orgId;
	private String clientName;
	private String businessUnit;
	private String uploadType;
	private String user;
	private BatchUserType userType;
	private String uuid;
	private boolean queued;
	private Integer recordCount;
	private Integer errorCount;
	private Long exportId;
	private BatchStatus exportStatus;
	private LocalDateTime startDateTime;
	private LocalDateTime endDateTime;
	public long getUploadId() {
		return uploadId;
	}
	public void setUploadId(long uploadId) {
		this.uploadId = uploadId;
	}
	public String getUploadName() {
		return uploadName;
	}
	public void setUploadName(String uploadName) {
		this.uploadName = uploadName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public BatchStatus getStatus() {
		return status;
	}
	public void setStatus(BatchStatus status) {
		this.status = status;
	}
	public BatchRunType getRunType() {
		return runType;
	}
	public void setRunType(BatchRunType runType) {
		this.runType = runType;
	}
	public BatchResult getResult() {
		return result;
	}
	public void setResult(BatchResult result) {
		this.result = result;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getUploadType() {
		return uploadType;
	}
	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public BatchUserType getUserType() {
		return userType;
	}
	public void setUserType(BatchUserType userType) {
		this.userType = userType;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public boolean isQueued() {
		return queued;
	}
	public void setQueued(boolean queued) {
		this.queued = queued;
	}
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public Integer getErrorCount() {
		return errorCount;
	}
	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}
	public Long getExportId() {
		return exportId;
	}
	public void setExportId(Long exportId) {
		this.exportId = exportId;
	}
	public BatchStatus getExportStatus() {
		return exportStatus;
	}
	public void setExportStatus(BatchStatus exportStatus) {
		this.exportStatus = exportStatus;
	}
	public LocalDateTime getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(LocalDateTime startDateTime) {
		this.startDateTime = startDateTime;
	}
	public LocalDateTime getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(LocalDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}
	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}
	
	
}
