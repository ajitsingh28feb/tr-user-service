package com.ajit.model;

import java.time.LocalDateTime;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.ajit.info.BatchResult;
import com.ajit.info.BatchRunType;
import com.ajit.info.BatchStatus;

@Document(collection = "batchReport")
public class BatchReport extends BaseDataObj{
	@Transient
	public static final String SEQUENCE_NAME = "batchReport_sequence";
	@Id
	private long reportId;
	@NotNull
	private String reportName;
	private BatchStatus status;
	private BatchRunType runType;
	private BatchResult result;
	private Long orgId;
	private Long trackerId;
	private String clientName;
	private String businessUnit;
	private String reportType;
	private String user;
	private boolean userOnly;
	private boolean queued;
	private Integer recordCount;
	private Integer processCount;
	private Map<String, Integer> batchStepData;
	private boolean inactive;
	private LocalDateTime startDateTime;
	private LocalDateTime endDateTime;
	
	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public BatchStatus getStatus() {
		return status;
	}
	public void setStatus(BatchStatus status) {
		this.status = status;
	}
	public BatchRunType getRunType() {
		return runType;
	}
	public void setRunType(BatchRunType runType) {
		this.runType = runType;
	}
	public BatchResult getResult() {
		return result;
	}
	public void setResult(BatchResult result) {
		this.result = result;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public Long getTrackerId() {
		return trackerId;
	}
	public void setTrackerId(Long trackerId) {
		this.trackerId = trackerId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public boolean isUserOnly() {
		return userOnly;
	}
	public void setUserOnly(boolean userOnly) {
		this.userOnly = userOnly;
	}
	public boolean isQueued() {
		return queued;
	}
	public void setQueued(boolean queued) {
		this.queued = queued;
	}
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public Integer getProcessCount() {
		return processCount;
	}
	public void setProcessCount(Integer processCount) {
		this.processCount = processCount;
	}
	public Map<String, Integer> getBatchStepData() {
		return batchStepData;
	}
	public void setBatchStepData(Map<String, Integer> batchStepData) {
		this.batchStepData = batchStepData;
	}
	public boolean isInactive() {
		return inactive;
	}
	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}
	public LocalDateTime getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(LocalDateTime startDateTime) {
		this.startDateTime = startDateTime;
	}
	public LocalDateTime getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(LocalDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}
	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}
	
	
	
}
