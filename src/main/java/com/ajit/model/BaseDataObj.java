package com.ajit.model;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

@Component
public class BaseDataObj {
	String operation;
	String crtUsrId;
	String vrsnId;
	LocalDateTime crtTs;
	LocalDateTime updTs;
	
	public BaseDataObj() {
		
	}
	
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getCrtUsrId() {
		return crtUsrId;
	}
	public void setCrtUsrId(String crtUsrId) {
		this.crtUsrId = crtUsrId;
	}
	public String getVrsnId() {
		return vrsnId;
	}
	public void setVrsnId(String vrsnId) {
		this.vrsnId = vrsnId;
	}
	public LocalDateTime getCrtTs() {
		return crtTs;
	}
	public void setCrtTs(LocalDateTime crtTs) {
		this.crtTs = crtTs;
	}
	public LocalDateTime getUpdTs() {
		return updTs;
	}
	public void setUpdTs(LocalDateTime updTs) {
		this.updTs = updTs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((crtTs == null) ? 0 : crtTs.hashCode());
		result = prime * result + ((crtUsrId == null) ? 0 : crtUsrId.hashCode());
		result = prime * result + ((operation == null) ? 0 : operation.hashCode());
		result = prime * result + ((updTs == null) ? 0 : updTs.hashCode());
		result = prime * result + ((vrsnId == null) ? 0 : vrsnId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseDataObj other = (BaseDataObj) obj;
		if (crtTs == null) {
			if (other.crtTs != null)
				return false;
		} else if (!crtTs.equals(other.crtTs))
			return false;
		if (crtUsrId == null) {
			if (other.crtUsrId != null)
				return false;
		} else if (!crtUsrId.equals(other.crtUsrId))
			return false;
		if (operation == null) {
			if (other.operation != null)
				return false;
		} else if (!operation.equals(other.operation))
			return false;
		if (updTs == null) {
			if (other.updTs != null)
				return false;
		} else if (!updTs.equals(other.updTs))
			return false;
		if (vrsnId == null) {
			if (other.vrsnId != null)
				return false;
		} else if (!vrsnId.equals(other.vrsnId))
			return false;
		return true;
	}
	
	
	
	
}

