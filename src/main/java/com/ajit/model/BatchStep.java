package com.ajit.model;

public enum BatchStep {
	PROCESS,
	VALIDATE;
	
	private BatchStep() {
		
	}
}
