package com.ajit.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.ajit.info.BatchAccessType;
import com.ajit.info.BatchJobType;
import com.ajit.info.BatchRunType;
import com.ajit.info.BatchStatus;
import com.ajit.info.BatchUserType;

@Document(collection = "batchConfig")
public class BatchConfig extends BaseDataObj {
	@Transient
	public static final String SEQUENCE_NAME = "batchConfig_sequence";
	@Id
	private long jobId;
	private String jobName;
	private String name;
	private String description;
	private BatchUserType userType;
	private BatchAccessType accessType;
	private BatchJobType jobType;
	private String orderType;
	private int threads;
	private int threadsVal;
	private int chunks;
	private int chunksVal;
	private BatchRunType runType;
	private BatchStatus status;
	private String message;
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BatchUserType getUserType() {
		return userType;
	}
	public void setUserType(BatchUserType userType) {
		this.userType = userType;
	}
	public BatchAccessType getAccessType() {
		return accessType;
	}
	public void setAccessType(BatchAccessType accessType) {
		this.accessType = accessType;
	}
	public BatchJobType getJobType() {
		return jobType;
	}
	public void setJobType(BatchJobType jobType) {
		this.jobType = jobType;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public int getThreads() {
		return threads;
	}
	public void setThreads(int threads) {
		this.threads = threads;
	}
	public int getThreadsVal() {
		return threadsVal;
	}
	public void setThreadsVal(int threadsVal) {
		this.threadsVal = threadsVal;
	}
	public int getChunks() {
		return chunks;
	}
	public void setChunks(int chunks) {
		this.chunks = chunks;
	}
	public int getChunksVal() {
		return chunksVal;
	}
	public void setChunksVal(int chunksVal) {
		this.chunksVal = chunksVal;
	}
	public BatchRunType getRunType() {
		return runType;
	}
	public void setRunType(BatchRunType runType) {
		this.runType = runType;
	}
	public BatchStatus getStatus() {
		return status;
	}
	public void setStatus(BatchStatus status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}
	
	
	
}
