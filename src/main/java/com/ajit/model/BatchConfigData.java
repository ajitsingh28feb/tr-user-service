package com.ajit.model;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.ajit.info.BatchJobType;
import com.ajit.info.BatchRunType;
import com.ajit.info.BatchStatus;

@Component
public class BatchConfigData {
	private String jobName;
	private Long batchTrackId;
	private Long uploadId;
	private Long reportId;
	private Integer threadCount;
	private Integer threadCountVal;
	private Integer threadChunk;
	private Integer threadChunkVal;
	private BatchRunType runType;
	private BatchJobType jobType;
	private String orderType;
	private BatchStatus status;
	private List<Map<String, String>> batchParameters;
	private Map<String, String> readParameter;
	private Map<String, String> writeParameter;
	private Map<String, String> criteriaParameter;
	private Query query;
	private Object queryModel;
	private String processor;
	private String user;
	private boolean batchMultiJob;
	private String fieldMapper;
	private Integer fileRowCount;
	private Integer recordCount;
	private Resource inputResource;
	
	
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Long getBatchTrackId() {
		return batchTrackId;
	}
	public void setBatchTrackId(Long batchTrackId) {
		this.batchTrackId = batchTrackId;
	}
	public Long getUploadId() {
		return uploadId;
	}
	public void setUploadId(Long uploadId) {
		this.uploadId = uploadId;
	}
	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	public Integer getThreadCount() {
		return threadCount;
	}
	public void setThreadCount(Integer threadCount) {
		this.threadCount = threadCount;
	}
	public Integer getThreadCountVal() {
		return threadCountVal;
	}
	public void setThreadCountVal(Integer threadCountVal) {
		this.threadCountVal = threadCountVal;
	}
	public Integer getThreadChunk() {
		return threadChunk;
	}
	public void setThreadChunk(Integer threadChunk) {
		this.threadChunk = threadChunk;
	}
	public Integer getThreadChunkVal() {
		return threadChunkVal;
	}
	public void setThreadChunkVal(Integer threadChunkVal) {
		this.threadChunkVal = threadChunkVal;
	}
	public BatchRunType getRunType() {
		return runType;
	}
	public void setRunType(BatchRunType runType) {
		this.runType = runType;
	}
	public BatchJobType getJobType() {
		return jobType;
	}
	public void setJobType(BatchJobType jobType) {
		this.jobType = jobType;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public BatchStatus getStatus() {
		return status;
	}
	public void setStatus(BatchStatus status) {
		this.status = status;
	}
	public List<Map<String, String>> getBatchParameters() {
		return batchParameters;
	}
	public void setBatchParameters(List<Map<String, String>> batchParameters) {
		this.batchParameters = batchParameters;
	}
	public Map<String, String> getReadParameter() {
		return readParameter;
	}
	public void setReadParameter(Map<String, String> readParameter) {
		this.readParameter = readParameter;
	}
	public Map<String, String> getWriteParameter() {
		return writeParameter;
	}
	public void setWriteParameter(Map<String, String> writeParameter) {
		this.writeParameter = writeParameter;
	}
	public Map<String, String> getCriteriaParameter() {
		return criteriaParameter;
	}
	public void setCriteriaParameter(Map<String, String> criteriaParameter) {
		this.criteriaParameter = criteriaParameter;
	}
	public Query getQuery() {
		return query;
	}
	public void setQuery(Query query) {
		this.query = query;
	}
	public Object getQueryModel() {
		return queryModel;
	}
	public void setQueryModel(Object queryModel) {
		this.queryModel = queryModel;
	}
	public String getProcessor() {
		return processor;
	}
	public void setProcessor(String processor) {
		this.processor = processor;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public boolean isBatchMultiJob() {
		return batchMultiJob;
	}
	public void setBatchMultiJob(boolean batchMultiJob) {
		this.batchMultiJob = batchMultiJob;
	}
	public String getFieldMapper() {
		return fieldMapper;
	}
	public void setFieldMapper(String fieldMapper) {
		this.fieldMapper = fieldMapper;
	}
	public Integer getFileRowCount() {
		return fileRowCount;
	}
	public void setFileRowCount(Integer fileRowCount) {
		this.fileRowCount = fileRowCount;
	}
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public Resource getInputResource() {
		return inputResource;
	}
	public void setInputResource(Resource inputResource) {
		this.inputResource = inputResource;
	}
	
	

}
