package com.ajit.model;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class BatchSequenceGenerator {
	private MongoOperations mongoOperations;
	
	@Autowired
	public BatchSequenceGenerator(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}
	
	public long generateSequence(String seqName) {
		BatchSequence counter = (BatchSequence) this.mongoOperations.findAndModify(Query.query(Criteria.where("_id").is(seqName)), (new Update()).inc("seq", 1), FindAndModifyOptions.options().returnNew(true), BatchSequence.class);
		return !Objects.isNull(counter) ? counter.getSeq() : 1L;
	}
}
