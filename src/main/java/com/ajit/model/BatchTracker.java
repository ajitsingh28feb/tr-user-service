package com.ajit.model;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.ajit.info.BatchJobType;
import com.ajit.info.BatchResult;
import com.ajit.info.BatchRunType;
import com.ajit.info.BatchStatus;

@Document(collection = "batchTracker")
public class BatchTracker extends BaseDataObj {
	@Transient
	public static final String SEQUENCE_NAME = "batchTracker_sequence";
	@Id
	private long trackId;
	@NotNull
	private long jobId;
	private long jobExecutionId;
	private String jobName;
	private BatchJobType jobType;
	private String org;
	private String user;
	private long uploadId;
	private long reportId;
	private BatchStatus status;
	private BatchRunType runType;
	private BatchResult result;
	private String path;
	private String source;
	private String destination;
	private String fileName;
	private LocalDateTime startDateTime;
	private LocalDateTime endDateTime;
	public long getTrackId() {
		return trackId;
	}
	public void setTrackId(long trackId) {
		this.trackId = trackId;
	}
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	public long getJobExecutionId() {
		return jobExecutionId;
	}
	public void setJobExecutionId(long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public BatchJobType getJobType() {
		return jobType;
	}
	public void setJobType(BatchJobType jobType) {
		this.jobType = jobType;
	}
	public String getOrg() {
		return org;
	}
	public void setOrg(String org) {
		this.org = org;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public long getUploadId() {
		return uploadId;
	}
	public void setUploadId(long uploadId) {
		this.uploadId = uploadId;
	}
	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public BatchStatus getStatus() {
		return status;
	}
	public void setStatus(BatchStatus status) {
		this.status = status;
	}
	public BatchRunType getRunType() {
		return runType;
	}
	public void setRunType(BatchRunType runType) {
		this.runType = runType;
	}
	public BatchResult getResult() {
		return result;
	}
	public void setResult(BatchResult result) {
		this.result = result;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public LocalDateTime getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(LocalDateTime startDateTime) {
		this.startDateTime = startDateTime;
	}
	public LocalDateTime getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(LocalDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}
	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}
	
	
	
	
}
