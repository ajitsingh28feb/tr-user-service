package com.ajit.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="batchReportData")
public class BatchReportData<T> extends BaseDataObj {
	@Transient
	public static final String SEQUENCE_NAME = "batchReportData_sequence";
	@Id
	private long reportDataId;
	@NotNull
	private long reportId;
	private T data;
	private String dataId;
	
	public long getReportDataId() {
		return reportDataId;
	}
	public void setReportDataId(long reportDataId) {
		this.reportDataId = reportDataId;
	}
	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public String getDataId() {
		return dataId;
	}
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}
	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}
	
	
	
}
