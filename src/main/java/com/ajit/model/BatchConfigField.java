package com.ajit.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.ajit.info.BatchAction;
import com.ajit.info.BatchTypeField;

@Document(collection = "batchConfigField")
public class BatchConfigField extends BaseDataObj {
	@Transient
	public static final SEQUENCE_NAME = "batchConfigField_sequence";
	@Id
	private long fieldId;
	@NotNull
	private long jobId;
	private long jobName;
	private BatchTypeField field;
	private BatchAction action;
	private String value;
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	public long getJobName() {
		return jobName;
	}
	public void setJobName(long jobName) {
		this.jobName = jobName;
	}
	public BatchTypeField getField() {
		return field;
	}
	public void setField(BatchTypeField field) {
		this.field = field;
	}
	public BatchAction getAction() {
		return action;
	}
	public void setAction(BatchAction action) {
		this.action = action;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public static long getFieldid() {
		return fieldId;
	}
	
	
}
