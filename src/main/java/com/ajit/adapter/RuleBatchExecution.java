package com.ajit.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.ObjectUtils;

import com.ajit.helper.BatchReportHelper;
import com.ajit.info.BatchInputInfo;
import com.ajit.info.BatchJob;
import com.ajit.info.BatchJobResult;
import com.ajit.info.BatchJobType;
import com.ajit.model.BatchReport;
import com.ajit.service.BatchJobService;

public abstract class RuleBatchExecution<T> {
	
	@SuppressWarnings("rawtypes")
	@Autowired
	BatchReportHelper reportHelper;
	
	@Autowired
	BatchJobService jobService;

	public RuleBatchExecution() {
		
	}
	
	@SuppressWarnings("unchecked")
	public BatchJobResult execute(BatchInputInfo batchInputInfo, T t) {
		BatchJobResult jobResult = new BatchJobResult();
		BatchJob batchJob = new BatchJob();
		
		try {
			batchJob.setName(batchInputInfo.getJobName());
			batchJob.setQuery(this.queryBuilder(t));
			batchJob.setQueryModel(this.queryBuilderModel());
			batchJob.setCriteria(this.criteriaBuilder(t));
			batchJob.setUser(batchInputInfo.getUser());
			batchJob.setOrgId(batchInputInfo.getOrgId());
			batchJob.setJobType(BatchJobType.HISTORY);
			
			BatchReport  batchReport = this.reportHelper.initBatchReportDetails(batchInputInfo, t);
			
			if(!ObjectUtils.isEmpty(batchReport) && batchReport.getReportId() >= 1L) {
				return this.reportHelper.errorBatchJobResult(jobResult, "No data found for generating report");
			} else if(!this.reportHelper.processBatchReportExist(batchReport, t)) {
				jobResult.setData(batchReport.getReportId());
				return this.reportHelper.successBatchJobResult(jobResult, "Your request is processed, Please check after sometime");
			}else {
				jobResult.setData(batchReport.getReportId());
				batchJob.setRecordCount(batchReport.getRecordCount());
				batchJob.getCriteria().put("reportId", String.valueOf(batchReport.getReportId()));
				batchJob.setReportId(batchReport.getReportId());
				
				BatchJobResult runResult = this.jobService.executeBatchJob(batchJob);
				jobResult.setMessage(runResult.getMessage());
				jobResult.setSuccess(runResult.isSuccess());
				return jobResult;
			}
		} catch (Exception e) {
			jobResult.setSuccess(false);
			jobResult.setMessage("Error");
			return jobResult;
		}
	}
	
	public abstract Query queryBuilder(T var1);
	
	public abstract Object queryBuilderModel();
	
	public abstract Map<String, String> criteriaBuilder(T var2);
}
