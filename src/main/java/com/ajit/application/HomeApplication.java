package com.ajit.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan(basePackages={"com.ajit"})
@EnableMongoRepositories("com.ajit.repository")
@EnableEurekaServer
@EnableEurekaClient
public class HomeApplication {
	public static void main(String ar[]) {
		SpringApplication.run(HomeApplication.class, ar);
	}
}
