package com.ajit.util;

import org.springframework.stereotype.Service;

import com.ajit.info.BatchInputInfo;

@Service
public class EmsReportUtil {
	
	public static BatchInputInfo initBatchInputInfo(String reportName,
													String reportType,
													Long orgId,
													String clientName) {
		BatchInputInfo batchInputInfo = new BatchInputInfo();
		batchInputInfo.setOrgId(orgId);
		batchInputInfo.setClientName(clientName);
		batchInputInfo.setJobName(reportName);
		batchInputInfo.setJobDisplayName(reportType);
		
		return batchInputInfo;
	}
	
}
