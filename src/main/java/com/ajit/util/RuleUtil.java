package com.ajit.util;

import com.ajit.info.ErrorResponse;
import org.springframework.stereotype.Component;

@Component
public class RuleUtil {
    public ErrorResponse getErrorResponse(int errorCode, String errorMsg) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(errorCode);
        errorResponse.setErrorMessage(errorMsg);
        return errorResponse;
    }
}
