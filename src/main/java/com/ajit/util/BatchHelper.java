package com.ajit.util;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajit.info.BatchStatus;
import com.ajit.model.BatchSequenceGenerator;
import com.ajit.model.BatchTracker;
import com.ajit.service.BatchTrackerService;

@Service
public class BatchHelper {
	
	@Autowired
	BatchTrackerService runTrackerService;
	
	@Autowired
	BatchSequenceGenerator sequenceGenerator;

	public BatchHelper() {
		
	}
	
	public BatchTracker updateJobTracker(long runId, BatchStatus status, String fileName) {
		BatchTracker runTracker = this.runTrackerService.findById(runId);
		if(status != null) {
			runTracker.setStatus(status);
		}
		
		if(fileName != null) {
			runTracker.setFileName(fileName);
		}
		
		runTracker.setEndDateTime(LocalDateTime.now());
		runTracker.setUpdTs(LocalDateTime.now());
		
		return this.runTrackerService.save(runTracker);
	}
	
	public BatchTracker createJobTracker(BatchTracker runTracker) {
		runTracker.setTrackId(this.sequenceGenerator.generateSequence("batchTracker_sequence"));
		runTracker.setStartDateTime(LocalDateTime.now());
		runTracker.setCrtTs(LocalDateTime.now());
		
		return this.runTrackerService.save(runTracker);
	}
}
