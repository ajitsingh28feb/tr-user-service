package com.ajit.util;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajit.info.BatchStatus;
import com.ajit.model.BatchReport;
import com.ajit.service.BatchReportService;

@Service
public class BatchReportHelper {
	
	@Autowired
	BatchReportService reportService;

	public BatchReport updateBatchReport(long uploadId, BatchStatus status) {
		BatchReport batchReport = new BatchReport();
		batchReport.setStatus(status);
		
		if(Arrays.asList(BatchStatus.COMPLETE, BatchStatus.ERROR).contains(status)) {
			batchReport.setEndDateTime(LocalDateTime.now());
		}
		
		batchReport.setUpdTs(LocalDateTime.now());
		return this.reportService.saveBatchReport(batchReport);
	}
}
