package com.ajit.util;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajit.info.BatchStatus;
import com.ajit.model.BatchUpload;
import com.ajit.service.BatchUploadService;

@Service
public class BatchUploadHelper {

	@Autowired
	BatchUploadService uploadService;
	
	public BatchUpload updateBatchUpload(long uploadId, BatchStatus status) {
		BatchUpload batchUpload = this.uploadService.findById(uploadId);
		batchUpload.setStatus(status);
		
		if(Arrays.asList(BatchStatus.COMPLETE, BatchStatus.ERROR).contains(status)) {
			batchUpload.setEndDateTime(LocalDateTime.now());
		}
		
		batchUpload.setUpdTs(LocalDateTime.now());
		return this.uploadService.save(batchUpload);
	}
}
