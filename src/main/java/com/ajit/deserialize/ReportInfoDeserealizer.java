package com.ajit.deserialize;

import java.io.IOException;

import com.ajit.exception.RecordNotFoundException;
import com.ajit.info.CommonReportInfo;
import com.ajit.info.RuleReportInfo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ReportInfoDeserealizer extends JsonDeserializer<ReportInfoI>{

	@Override
	public ReportInfoI deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectMapper mapper = (ObjectMapper) p.getCodec();
		Object parent = ctxt.getParser().getParsingContext().getParent().getCurrentValue();
		
		CommonReportInfo reportInfoCommon = (CommonReportInfo) parent;
		String reportType = reportInfoCommon.getReportType();
		ObjectNode root = mapper.readTree(p);
		
		if(reportType.equalsIgnoreCase("RuleReport")) {
			return mapper.readValue(root.toString(), RuleReportInfo.class);
		} else{
			throw new RecordNotFoundException("Report not exist");
		}
	}

}
