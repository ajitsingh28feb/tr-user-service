package com.ajit.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ajit.model.BatchReport;
import com.ajit.model.BatchReportData;

@Repository
public interface BatchReportRepository extends MongoRepository<BatchReport, Long> {
	List<BatchReport> findAllByStatus(String status);
	
	BatchReportData findFirstByDataId(String dataId);
}
