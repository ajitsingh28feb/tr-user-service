package com.ajit.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ajit.model.BatchUpload;

@Repository
public interface BatchUploadRepository extends MongoRepository<BatchUpload, Long> {
	List<BatchUpload> findAllByStatus(String status);
}
