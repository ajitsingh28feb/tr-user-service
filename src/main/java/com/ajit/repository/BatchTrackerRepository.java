package com.ajit.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ajit.model.BatchTracker;

@Repository
public interface BatchTrackerRepository extends MongoRepository<BatchTracker, Long>{

	BatchTracker findByJobId(long jobId);
	
	List<BatchTracker> findAllByOrgAndUser(String org, String user);
	
	BatchTracker findUploadId(Long uploadId);
	
	BatchTracker findByReportId(Long reportId);
}
