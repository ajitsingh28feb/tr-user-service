package com.ajit.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ajit.model.BatchConfig;

@Repository
public interface BatchConfigRepository extends MongoRepository<BatchConfig, String>{
	BatchConfig findByJobNameAndRunType(String jobName, String runType);
	
	BatchConfig findByJobNameAndStatus(String jobName, String status);
	
	List<BatchConfig> findAllByJobTypeAndUserTypeAndStatus(String jobType, String userType, String status);
}
