package com.ajit.info;

public enum BatchRunType {
	INLINE,
	OFFLINE;
	
	private BatchRunType() {
		
	}
}
