package com.ajit.info;

import com.ajit.deserialize.ReportInfoI;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonReportInfo {
	
	String reportType;
	
	ReportInfoI reportInfo;

	public CommonReportInfo() {
		
	}
	
	public CommonReportInfo(String reportType, ReportInfoI reportInfo) {
		super();
		this.reportType = reportType;
		this.reportInfo = reportInfo;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public ReportInfoI getReportInfo() {
		return reportInfo;
	}

	public void setReportInfo(ReportInfoI reportInfo) {
		this.reportInfo = reportInfo;
	}
	
	

}
