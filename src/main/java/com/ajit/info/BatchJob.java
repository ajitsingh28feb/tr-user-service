package com.ajit.info;

import java.util.Map;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
public class BatchJob {
	private String name;
	private Query query;
	private Object queryModel;
	private Map<String, String> criteria;
	private String user;
	private Long orgId;
	private Long uploadId;
	private Long reportId;
	private boolean batchMultiJob;
	private Integer fileRowCount;
	private Integer recordCount;
	private BatchJobType jobType;
	private String exportHeaderJobName;
	private String exportObjJobName;
	private boolean readSPOS;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Query getQuery() {
		return query;
	}
	public void setQuery(Query query) {
		this.query = query;
	}
	public Object getQueryModel() {
		return queryModel;
	}
	public void setQueryModel(Object queryModel) {
		this.queryModel = queryModel;
	}
	public Map<String, String> getCriteria() {
		return criteria;
	}
	public void setCriteria(Map<String, String> criteria) {
		this.criteria = criteria;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public Long getUploadId() {
		return uploadId;
	}
	public void setUploadId(Long uploadId) {
		this.uploadId = uploadId;
	}
	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	public boolean isBatchMultiJob() {
		return batchMultiJob;
	}
	public void setBatchMultiJob(boolean batchMultiJob) {
		this.batchMultiJob = batchMultiJob;
	}
	public Integer getFileRowCount() {
		return fileRowCount;
	}
	public void setFileRowCount(Integer fileRowCount) {
		this.fileRowCount = fileRowCount;
	}
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public BatchJobType getJobType() {
		return jobType;
	}
	public void setJobType(BatchJobType jobType) {
		this.jobType = jobType;
	}
	public String getExportHeaderJobName() {
		return exportHeaderJobName;
	}
	public void setExportHeaderJobName(String exportHeaderJobName) {
		this.exportHeaderJobName = exportHeaderJobName;
	}
	public String getExportObjJobName() {
		return exportObjJobName;
	}
	public void setExportObjJobName(String exportObjJobName) {
		this.exportObjJobName = exportObjJobName;
	}
	public boolean isReadSPOS() {
		return readSPOS;
	}
	public void setReadSPOS(boolean readSPOS) {
		this.readSPOS = readSPOS;
	}
	
		
}
