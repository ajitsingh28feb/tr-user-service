package com.ajit.info;

public enum BatchUserType {
	INTERNAL,
	EXTERNAL,
	BOTH;
	
	private BatchUserType() {
		
	}
}
