package com.ajit.info;

public enum RuleStatus {
	ACTIVE,
	DRAFT,
	EXPIRED,
	SCHEDULED;
}
