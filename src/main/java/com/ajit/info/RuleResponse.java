package com.ajit.info;

import lombok.Data;

@Data
public class RuleResponse<T> {

	private String message;
	private T data;
	private ErrorResponse errorResponse;
	
	public RuleResponse() {
		
	}
	
	
	public RuleResponse(String message, T data, ErrorResponse errorResponse) {
		super();
		this.message = message;
		this.data = data;
		this.errorResponse = errorResponse;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public ErrorResponse getErrorResponse() {
		return errorResponse;
	}
	public void setErrorResponse(ErrorResponse errorResponse) {
		this.errorResponse = errorResponse;
	}
	
	
}
