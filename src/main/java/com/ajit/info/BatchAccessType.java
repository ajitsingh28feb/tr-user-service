package com.ajit.info;

public enum BatchAccessType {
	ALL,
	CLIENT;
	
	private BatchAccessType() {
		
	}
}
