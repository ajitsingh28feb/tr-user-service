package com.ajit.info;

public enum BatchStatus {
	RECEIVED,
	ACTIVE,
	INACTIVE,
	STARTED,
	ERROR,
	COMPLETE;
	
	private BatchStatus() {
		
	}
}
