package com.ajit.info;

public enum BatchAction {
	READER,
	WRITER,
	PROCESSOR,
	FIELD_MAPPER,
	UPLOAD,
	REPORT;
	
	private BatchAction() {
		
	}
}
