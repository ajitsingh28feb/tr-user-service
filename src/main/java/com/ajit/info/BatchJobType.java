package com.ajit.info;

public enum BatchJobType {
	HISTORY,
	SNAPSHOT,
	UPLOAD,
	EXPORT;
	
	private BatchJobType() {
		
	}
}
