package com.ajit.info;

import java.time.LocalDate;

import org.apache.commons.codec.language.bm.RuleType;

import com.ajit.deserialize.ReportInfoI;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize(as = RuleReportInfo.class)
public class RuleReportInfo implements ReportInfoI{

	private Long orgId;
	private String clientId;
	private String clientName;
	private LocalDate startDate;
	private LocalDate endDate;
	private String ruleName;
	private RuleType ruleType;
	private RuleStatus status;
	private String cusip;
	private String symbol;
	
	public RuleReportInfo() {
		
	}
	
	public RuleReportInfo(Long orgId, String clientId, String clientName, LocalDate startDate, LocalDate endDate,
			String ruleName, RuleType ruleType, RuleStatus status, String cusip, String symbol) {
		super();
		this.orgId = orgId;
		this.clientId = clientId;
		this.clientName = clientName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.ruleName = ruleName;
		this.ruleType = ruleType;
		this.status = status;
		this.cusip = cusip;
		this.symbol = symbol;
	}
	
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public RuleType getRuleType() {
		return ruleType;
	}
	public void setRuleType(RuleType ruleType) {
		this.ruleType = ruleType;
	}
	public RuleStatus getStatus() {
		return status;
	}
	public void setStatus(RuleStatus status) {
		this.status = status;
	}
	public String getCusip() {
		return cusip;
	}
	public void setCusip(String cusip) {
		this.cusip = cusip;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	
	
}
