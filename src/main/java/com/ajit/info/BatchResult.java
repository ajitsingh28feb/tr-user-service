package com.ajit.info;

public enum BatchResult {
	SUCCESS,
	FAIL;
	
	private BatchResult() {
		
	}
}
