package com.ajit.info;

import org.springframework.stereotype.Component;

@Component
public class BatchInputInfo {
	private String jobName;
	private String jobDisplayName;
	private Long orgId;
	private String clientName;
	private String businessUnit;
	private Integer recordCount;
	private String user;
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobDisplayName() {
		return jobDisplayName;
	}
	public void setJobDisplayName(String jobDisplayName) {
		this.jobDisplayName = jobDisplayName;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	
	
	
}
