package com.ajit.info;

public enum BatchTypeField {

	QUERY,
	MODEL,
	MAPPER,
	COLLECTION,
	HEADER,
	FILENAME,
	FIELDS,
	DELIMITER,
	TRACKID,
	FORMAT,
	PATH,
	FILEPATH,
	SORT;
	
	private BatchTypeField() {
		
	}
}
