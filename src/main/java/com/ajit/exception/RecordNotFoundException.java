package com.ajit.exception;

public class RecordNotFoundException extends  RuntimeException {
    public RecordNotFoundException(String message) {
        super(message);
    }
}
